# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018-2020 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


run-help = "    run : Run an NGINX podman image to test the website locally."

.PHONY: run
run :
	test $$(which podman) && $(PODMAN_CMD) || $(DOCKER_CMD)
