# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018-2020 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# html target for use with Skabelon and Jinja2 source templates

#   Add "git+git://github.com/fretboardfreak/skabelon.git@master" to the
#     $(PYTHON_REQUIRES) file and rerun the install-py target to enable.

html-help = "    html : Use skabelon to build the HTML pages."

html : dist
	$(PYVENV)/bin/skabelon -vd --templates $(HTML) \
		--dispatch $(SKABELON_DISPATCH) \
		--dispatch-opt host:$(HOST)
	date > html
