# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018-2020 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


clean-css-help = "    clean-css : Remove any built CSS files."
css-help = "    css : Run both css-compile and css-prefix."
css-compile-help = "    css-compile : compile from SCSS to CSS using SASS."
css-prefix-help = "    css-prefix : prefix the CSS sheet using postcss autoprefixer."
cssdist-help = "    cssdist : Make the css directory in the DIST directory."


.PHONY: clean-css
clean-css :
	rm -rf $(CSS_BUILD) $(CSS_DIST) css-compile css-prefix css cssdist

cssdist : dist
	mkdir -p $(CSS_DIST)
	date > cssdist

css-compile : build
	for stylesheet in $$(find $(CSS) -iname "*.scss"); do npx --prefix $(NODE_PKG) node-sass --include-path $(BOOTSTRAP_SCSS) $$stylesheet -o $(CSS_BUILD); done
	date > css-compile

css-prefix : dist cssdist css-compile
	npx --prefix $(NODE_PKG) postcss --use autoprefixer --dir $(CSS_DIST) $(CSS_BUILD)
	date > css-prefix

css : css-compile css-prefix
	date > css
