# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

TITLE = Website Repository

# HOST: use localhost for local testing, use your domain for production.
HOST = http://localhost

# DEPLOY_DEST: change this to use the deploy targets to deploy the website.
DEPLOY_DEST = /tmp/website

BUILD = $$(pwd)/build
DIST = $$(pwd)/dist

# IMAGES var should end in "images" to be compatible
IMAGES = $$(pwd)/images
SRC = $$(pwd)/src
HTML = $(SRC)/pages
JS = $(SRC)/js
JS_DIST = $(DIST)/js
CSS = $(SRC)/css
CSS_BUILD = $(BUILD)/css
CSS_DIST = $(DIST)/css
STATIC = $(SRC)/static
STATIC_DIST = $(DIST)
NODE = $$(pwd)/node_modules
NODE_PKG = $$(pwd)
PYTHON_REQUIRES = $$(pwd)/requirements.txt
PYVENV = $$(pwd)/venv
SKABELON_DISPATCH = $$(pwd)/src/skabelon/dispatch_templates.py
BOOTSTRAP_SCSS = $(NODE)/bootstrap/scss
BOOTSTRAP_JS = $(NODE)/bootstrap/dist/js/bootstrap.min.js
JQUERY_JS = $(NODE)/jquery/dist/jquery.min.js
ESLINT = $(NODE)/eslint/bin/eslint.js
RSYNC_OPTS = -haLP --no-whole-file --inplace

CONTAINER_OPTS = --rm -p 8888:80 -v $(DIST):/usr/share/nginx/html:Z -v $(IMAGES):/usr/share/nginx/html/images:Z
PODMAN_CMD = podman run $(CONTAINER_OPTS) docker.io/library/nginx:latest
DOCKER_CMD = sudo docker run $(CONTAINER_OPTS) nginx:latest

MAKE_DIR = make.d


include $(MAKE_DIR)/install.mk
include $(MAKE_DIR)/deploy-no-images.mk
include $(MAKE_DIR)/deploy-images.mk
include $(MAKE_DIR)/run.mk
include $(MAKE_DIR)/all.mk
include $(MAKE_DIR)/rebuild.mk
include $(MAKE_DIR)/clean.mk
include $(MAKE_DIR)/build.mk
include $(MAKE_DIR)/dist.mk
include $(MAKE_DIR)/files.mk
include $(MAKE_DIR)/html.mk
include $(MAKE_DIR)/css.mk
include $(MAKE_DIR)/js.mk

# Compile a Help Text with messages about each target.
HELP_TXT = \
	$(install-help)\n\
	$(install-npm-help)\n\
	$(install-py-help)\n\
	$(python-venv-help)\n\
	$(deploy-help)\n\
	$(deploy-images-help)\n\
	$(run-help)\n\
	$(all-help)\n\
	$(rebuild-help)\n\
	$(clean-help)\n\
	$(clean-all-help)\n\
	$(clean-build-help)\n\
	$(clean-dist-help)\n\
	$(clean-npm-help)\n\
	$(clean-py-help)\n\
	$(clean-js-help)\n\
	$(clean-css-help)\n\
	$(build-help)\n\
	$(dist-help)\n\
	$(static-help)\n\
	$(images-help)\n\
	$(html-help)\n\
	$(css-help)\n\
	$(cssdist-help)\n\
	$(css-compile-help)\n\
	$(css-prefix-help)\n\
	$(js-help)\n\
	$(jsdist-help)\n\
	$(jquery-help)\n\
	$(bootstrap-help)\n\
	$(jslint-help)\n

include $(MAKE_DIR)/help.mk
